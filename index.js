// Javascript Array

// Array basic structure
/* Syntax:
	let/const arrayName = [elementA, elementB, elementC ...];
*/

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

// Possible use of mixed elements in an array but is not recommended
let mixedArr = [12, 'Asus', null, undefined, true];

let myTasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
];

// Accessing elements in an array
console.log(grades);
console.log(computerBrands[2], computerBrands[7]);
console.log(computerBrands[4]);

// Reassign array values
console.log('Array before reassignment');
console.log(myTasks);
myTasks[0] = 'hello world';
console.log('Array after reassignment');
console.log(myTasks);

// Array Methods

// Mutator Methods
// - are functions that 'mutate' or change an array after they're created 

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

// push()
// - adds an element in the end of an array & returns the array's length
/* Syntax:
	arrayName.push();
*/
console.log('Current array:');
console.log(fruits);
let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated array from push method:');
console.log(fruits);


// pop()
// - remove the last element in an array & returns the removed element
/* Syntax:
	arrayName.pop();
*/
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method:');
console.log(fruits);

// unshift()
/*
	- adds one or more elements at the beginning of an array
	- syntax:
		arrayName.unshift('elementA');
		arrayName.unshift('elementA', 'elementB');
*/

fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift method:');
console.log(fruits);

// shift()
/*
	- removes an element at the beginning of an array & returns the removed element
	- Syntax:
		arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method:');
console.log(fruits);

// splice()
/*
	-simultaneously removes elements from a specified index number & adds elements 
	-Syntax:
		arrayName.splice(startingIndex, deleteCount, elementToBeAdded)
*/
fruits.splice(1, 2, 'Lime', 'Cherry', 'Durian');
console.log('Mutated array from splice method:');
console.log(fruits);

// Banana, Apple, Orange. Kiwi, Dragon Fruit

// sort()
/*
	- rearranges the array elements in alphanumeric order
	- Syntax:
		arrayName.sort();
*/

fruits.sort();
console.log('Mutated array from sort method:');
console.log(fruits);

// reverse()
/*
	- reverses the order of array elements:
	- Syntax:
		arrayName.reverse();
*/

fruits.reverse();
console.log('Mutated array from reverse method:');
console.log(fruits);

// Non-mutator methods
// - these are functions that do not modify or change an array after they're created

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

// indexOf()
/*
	- returns an index number of the first matching element found in an array
	- if no match was found, the result will be -1.
	- Syntax: 
		arrayName.indexOf(searchValue);
*/
let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method: ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf method: ' + invalidCountry);

// lastIndexOf
/*
	- returns the index number of the last matching element found in an array
	- Syntax:
		arrayName.lastIndexOf(searchValue);
		araryName.lastIndexOf(searchValue, fromIndex)
*/
// Getting the index number starting from the last element
let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method:' + lastIndex);

// Getting the index number starting from a specified index
let lastIndexStart = countries.lastIndexOf('PH', 6);
console.log('Result of lastIndexOf method: ' + lastIndexStart);

// slice()
/*
	- portions/slices elements from an array & returns a new array
	- Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
*/

console.log(countries);

// slicing off elements from a specified index tot he last element
let slicedArrayA = countries.slice(2);
console.log('Result from slice method: ');
console.log(slicedArrayA);

// slicing off elements from a specified index to another index
let slicedArrayB = countries.slice(2,4);
console.log('Result from slice method:');
console.log(slicedArrayB);

// toString()
/*
	- returns an array as a string separated by commas
	- Syntax:
		arrayName.toString();
*/

let stringArray = countries.toString();
console.log('Result from toString method:');
console.log(stringArray);

// concat()
/*
	- combines two arrays & returns the combined result
	- Syntax:
		arrayA.concat(arrayB);
		arrayA.concat(elementA);
*/

let tasksArrayA = ['drink html', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breathe sass'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat method:');
console.log(tasks);

// Combining multiple arrays
console.log('Result from concat method:');
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

// Combining array with elements
let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log('Result from concat method:');
console.log(combinedTasks);

// join()
/*
	- returns an array as a string separated by specified separator strings
	- Syntax:
		arrayName.join('separatorString')
*/
let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log(users.join());
console.log(users.join('-'));